import {
	setToken,
	removeToken,
	getToken
} from '@/utils/index.js'

import store from '@/store/index.js'

let baseURL = store.state.user.baseURL + 'api/'
// 	根据环境变量改变请求接口统一前缀
if (process.env.NODE_ENV === 'development') {
	//	开发环境
	// baseURL = 'http://pray.veac.cn/api/'
} else {
	//	生产环境
}

// const token = store.state.login.token;

/** 
 * post方法，对应post请求 
 * @param {String} url [请求的url地址] 
 * @param {Object} params [请求时携带的参数] 
 */
export function post(url, params) {
	uni.showLoading({
		title: '加载中...'
	});
	return new Promise((resolve, reject) => {
		uni.request({
			url: baseURL + url,
			data: params,
			header: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
				// 'accessToken': getToken()
				'accessToken': store.state.login.token
			},
			method: "POST",
			success: (response) => {
				let res = response.data
				// if (res.code === 200) {
				if (res.code === 0) {
					resolve(res)
				} else {
					reject(res)
					showError(res);
				}
				uni.hideLoading();
			},
			fail: (error) => {
				if (error && error.response) {
					reject(error.response)
					showError(error.response);
				}
				uni.hideLoading();
			},
		});
	})
}

/**
 * get方法，对应get请求
 * @param {String} url [请求的url地址]
 * @param {Object} params [请求时携带的参数]
 */
export function get(url, params) {
	uni.showLoading({
		title: '加载中...'
	});
	return new Promise((resolve, reject) => {
		uni.request({
			url: baseURL + url,
			data: params,
			header: {
				'Accept': 'application/json',
				'Content-Type': 'application/x-www-form-urlencoded', //自定义请求头信息
				// 'accessToken': getToken()
				'accessToken': store.state.login.token
			},
			method: "GET",
			success: (response) => {
				let res = response.data
				// if (res.code === 200) {
				if (res.code === 0) {
					resolve(res)
				} else {
					reject(res)
					showError(res);
				}
				uni.hideLoading();
			},
			fail: (error) => {
				if (error && error.response) {
					reject(error.response)
					showError(error.response);
				}
				uni.hideLoading();
			}
		});
	})
}

const showError = error => {
	let errorMsg = ''
	switch (error.code) {
		case 301:
			errorMsg = '未授权，请登录'
			break
		case 400:
			errorMsg = '请求参数错误'
			break
		case 401:
			store.commit('login/removeToken')
			errorMsg = '登录失效'
			store.commit('login/login')
			break
		case 403:
			errorMsg = '跨域拒绝访问'
			break
		case 404:
			errorMsg = '请求地址不存在'
			break
		case 406:
			if (error.msg == '实名认证信息不存在') {
				return
			} else if (error.msg == '用户不存在') {
				errorMsg = error.msg
				store.commit('login/removeToken')
			} else {
				errorMsg = error.msg
			}
			break
		case 409:
			errorMsg = error.msg
			if (error.msg == '请授权登录') {
				store.commit('login/removeToken')
			}
			break
		case 500:
			errorMsg = '服务器内部错误'
			break
		case 504:
			errorMsg = '请求超时'
			break
		default:
			errorMsg = error.msg
			break
	}
	console.log(error);
	if (errorMsg == '') errorMsg = '加载中...'
	uni.showToast({
		title: errorMsg,
		icon: 'none',
		duration: 3000,
		complete: function() {
			setTimeout(function() {
				uni.hideToast();
			}, 3000);
		}
	});
}