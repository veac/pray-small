import api from '@/api/index.js'

// 设置Token
export const setToken = (token) => {
	uni.setStorageSync('token', token);
}
// 获取Token
export const getToken = () => {
	return uni.getStorageSync('token') || ''
}
// 删除Token
export const removeToken = () => {
	uni.removeStorageSync('token');
}

export const pay = (order_id, order_type, fn) => {
	api.paywxpay({
		order_id,
		order_type
	}).then(res => {
		uni.requestPayment({
			provider: "wxpay", //固定值为"wxpay"
			orderInfo: {
				appid: "wxabe696a5fa941c35", // 应用ID（AppID）
				// partnerid: "1674337151",      // 商户号（PartnerID）
				nonceStr: res.data.nonceStr,
				prepayid: res.data.prepay_id, // 预支付交易会话ID
				package: 'prepay_id=' + res.data.prepay_id, // 固定值
				timestamp: res.data.timeStamp, // 时间戳（单位：秒）
				paySign: res.data.paySign // 签名，这里用的 MD5 签名
			},
			timeStamp: res.data.timeStamp,
			nonceStr: res.data.nonceStr,
			package: 'prepay_id=' + res.data.prepay_id,
			paySign: res.data.paySign,
			signType: "MD5",
			success: function(res) {
				console.log("支付成功", res);
				if(fn != undefined) fn()
			},
			fail: function(err) {
				console.log('支付失败:' + JSON.stringify(err));
				// if(fn != undefined) fn()
			}
		});
	})

}

