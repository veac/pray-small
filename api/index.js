import home from '@/api/modules/home.js'
import login from '@/api/modules/login.js'
import my from '@/api/modules/my.js'
import pary from '@/api/modules/pary.js'
import order from '@/api/modules/order.js'

export default {
	...home,
	...login,
	...my,
	...pary,
	...order
}