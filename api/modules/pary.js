import {
	post,
	get
} from "@/utils/request.js"

export default {
	templelist: data => post('temple/list', data),
	getlamplist: params => get('temple/getlamplist', params),
	templeInfo: params => get('temple/info', params),
	lightsupplylist: params => get('lightsupply/list', params),
	lightsupplycatelist: params => get('lightsupply/catelist', params),
	praysettinglist: data => post('praysetting/list', data),
	blessingordercreate: data => post('blessingorder/create', data),
	blessingordercancel: data => post('blessingorder/cancel', data),
	blessingorderdel: data => post('blessingorder/del', data),
	blessingorderinfo: params => get('blessingorder/info', params),
}