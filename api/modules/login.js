import {
	post,
	get
} from "@/utils/request.js"

export default {
	// 登录
	login: data => post('member/getTokenByCode', data),
	paywxpay: data => post('pay/wxpay', data),
}