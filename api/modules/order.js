import {
	post,
	get
} from "@/utils/request.js"

export default {
	synqueryindex: data => get('synquery/index', data),
	// deladdress: data => post('address/del', data),
	// blessingorderlist: data => post('blessingorder/list', data),
	// addresssave: data => post('address/save', data),
	// productOrderlist: data => post('productOrder/list', data),
	// productOrdercancel: data => post('productOrder/cancel', data),
	// productOrderdel: data => post('productOrder/del', data),
	productOrderinfo: params => get('productOrder/info', params),
	blessingorderinfo: params => get('blessingorder/info', params),
	aftersalegetreason: params => get('aftersale/getreason', params),
	aftersaleApply: params => post('aftersale/apply', params),
}