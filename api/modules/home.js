import {
	post,
	get
} from "@/utils/request.js"

export default {
	productlist: data => post('product/list', data),
	banner: params => get('/index/banner', params),
	minisystem: params => get('/index/minisystem', params),
	productinfo: params => get('product/info', params),
	addressinfo: params => get('address/info', params),
	productOrdercreate: data => post('productOrder/create', data),
	productOrderinfo: params => get('productOrder/info', params),
	noticelist: data => post('notice/list', data),
	meritList: params => get('member/meritList', params),
}