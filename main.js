import Vue from 'vue'
import App from './App'
import uView from '@/uni_modules/uview-ui'
import api from "@/api/index.js"
import store from '@/store/index.js'

import './uni.promisify.adaptor'

Vue.config.productionTip = false

Vue.use(uView);

Vue.prototype.$api = api;

App.mpType = 'app'

const app = new Vue({
	...App,
	store
})
app.$mount()