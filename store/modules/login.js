import { getToken, setToken, removeToken } from '@/utils/index.js'
import api from '@/api/index.js'

const state = {
	token: getToken() || ''
}

const mutations = {
	setToken(state, Token){
		state.token = Token
		setToken(Token)
	},
	removeToken(state){
		removeToken()
		state.token = ''
	},
	login(state){
		uni.showModal({
			title: '提示',
			content: '微信一键登录',
			success: function(res) {
				if (res.confirm) {
					
					if (getToken()) return
					uni.showLoading({
						title: '登录中...'
					});
					uni.login({
						provider: 'weixin', //使用微信登录
						success: function(loginRes) {
							api.login({
								code: loginRes.code
							}).then(res => {
								state.token = res.data
								setToken(res.data)
								uni.hideLoading()
							})
						}
					});
		
				} else if (res.cancel) {
					console.log('用户点击取消');
					uni.navigateBack()
				}
			}
		});
	}
}

const actions = {}

const getters = {}

export default {
	namespaced: true,
	state,
	mutations,
	actions,
	getters
}
