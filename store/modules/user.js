import { getToken } from '@/utils/index.js'
const state = {
	baseURL: 'https://pray.veac.cn/',
}

const mutations = {
	
}

const actions = {}

const getters = {}

export default {
	namespaced: true,
	state,
	mutations,
	actions,
	getters
}
